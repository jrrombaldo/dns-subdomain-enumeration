package br.jrrombaldo.enumeration.subdomain;

/**
 * Subdomain enumeration based using Bing engine. This is a extension of Google
 * Subdomain enumerations, so further configuration can be found directly on
 * Google class
 * 
 */
public class BingSearch extends GoogleSearch {

    public BingSearch(final String domain, final int maxPage) {
	super(domain, 5);
	this.set_urlStr("http://br.bing.com/search?first={1}&q={0}");
	this.setDorkNotIn("-sinte:");
	this.maxPage = maxPage;
    }
}
