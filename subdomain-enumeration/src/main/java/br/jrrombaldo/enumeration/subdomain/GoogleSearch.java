package br.jrrombaldo.enumeration.subdomain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Search subdomains using google advanced search (known as google dorks).
 */
public class GoogleSearch {

    /*
     * Uuser Agent used on connection, this is important because google return
     * plain HTML with make possible parse the results. ## Don't touch this ##
     */
    protected String userAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)";

    /*
     * Google URL used to query
     */
    protected String _urlStr = "https://www.google.com.br/search?start={1}&q={0}";

    /*
     * Dorks used to search new results and excludes subdomains found
     */
    protected String dorkIn = "site:";
    protected String dorkNotIn = "-inurl:";

    /*
     * Max number of subdomains found, consequently max dorks used
     */
    protected int maxDork = 30;

    /*
     * When found this number of empty pages consequently, the search is
     * considered done.
     */
    protected int maxEmptyPage = 2;

    protected int maxPage;

    /*
     * Number of results per page.
     */
    protected int pageSize = 10;

    /*
     * Regex used to get the result from the HTML page (result)
     */
    protected String regex = "(?!%3(A|a))[\\d\\w_]+\\.";

    /*
     * Common attributes
     */
    protected Set<String> domains;
    protected String domain;

    /*
     * Use of HTTP Proxy
     */
    private boolean useProxy;

    /**
     * 
     * @param domain
     *            The target domain, for example "w3schools.com"
     * @param maxPage
     *            number of pages used per query, this impacts directly in
     *            performance
     */
    public GoogleSearch(final String domain, final int maxPage) {
	this.domains = new HashSet<String>();
	this.domain = domain;
	this.regex = new StringBuilder().append(this.regex)
		.append(domain.replace(".", "\\.")).toString();

	this.useProxy = false;
	this.maxPage = maxPage;
    }

    /**
     * 
     * Perform subdomain enumeration
     * 
     * @return List of all subdomain found.
     * @throws Exception
     */
    public Set<String> getAllSubDomains() throws Exception {
	int total;
	do {
	    total = extractDomains(0);

	    /*
	     * When no more new results on actual page, move to nexts.
	     */
	    if (total == 0) {
		int emptyPage = 0;
		for (int pg = 0; pg < maxPage && emptyPage < maxEmptyPage; pg++) {
		    total = extractDomains(pg * pageSize);

		    /*
		     * counting the empty pages per query
		     */
		    if (total == 0) {
			emptyPage++;
		    } else {
			emptyPage = 0;
		    }
		}
	    }

	} while (total > 0);

	return this.domains;
    }

    /**
     * Extract results based on page (result of prior query)
     * 
     * @param page
     *            Number of page to be used
     * @return The number of new subdomains found
     * @throws Exception
     */
    protected int extractDomains(final int page) throws Exception {
	final String content = getContent(page);

	final Pattern pattern = Pattern.compile(this.regex);
	final Matcher matcher = pattern.matcher(content);

	final int sizeAntes = this.domains.size();

	while (matcher.find()) {
	    String fnd = matcher.group();
	    if (fnd.startsWith("3A")) {
		fnd = fnd.replaceFirst("3A", "");
	    }
	    if (fnd.startsWith("3a")) {
		fnd = fnd.replaceFirst("3a", "");
	    }
	    this.domains.add(fnd);
	}

	final int founds = (this.domains.size() - sizeAntes);

	return founds;
    }

    /**
     * Make the URL using the dorks for the next query. To mount the URL is
     * considered the subdomains found until now in order to exclude them from
     * next queries
     * 
     * @param page
     *            Number of page from actual query
     * @return
     * @throws Exception
     */
    protected String mountUrl(final int page) throws Exception {
	final StringBuilder sb = new StringBuilder();
	sb.append(this.dorkIn);
	sb.append(this.domain);

	int count = 0;
	for (final String subd : this.domains) {
	    sb.append("+");
	    sb.append(this.dorkNotIn);
	    sb.append(subd);
	    count++;
	    if (count >= maxDork) {
		break;
	    }
	}

	return MessageFormat.format(this._urlStr, sb.toString(), page);
    }

    /**
     * Extract and parse the page content of the result, in other words getting
     * the plain HTML returned by search engine.
     * 
     * @param page
     *            Number of page
     * @return Content of page (plain HTML)
     * @throws Exception
     */
    protected String getContent(final int page) throws Exception {
	confidInavlidSSL();

	final HttpURLConnection con = getUrlConnection(page);

	final BufferedReader br = new BufferedReader(new InputStreamReader(
		con.getInputStream()));

	final StringBuilder sb = new StringBuilder();

	String line;
	try {
	    while ((line = br.readLine()) != null) {
		sb.append(line);
	    }
	} catch (final IOException e) {
	    e.printStackTrace();
	}

	return sb.toString();
    }

    /**
     * Creating a HTML connection with the search engine
     * 
     * @param page
     *            number of page
     * @return The HTTP Connection opened
     * @throws MalformedURLException
     * @throws Exception
     * @throws IOException
     */
    private HttpURLConnection getUrlConnection(final int page)
	    throws MalformedURLException, Exception, IOException {
	final URL url = new URL(mountUrl(page));
	URLConnection ucon;
	if (this.useProxy) {
	    final Proxy proxy = new Proxy(Proxy.Type.HTTP,
		    new InetSocketAddress("localhost", 8989));

	    ucon = url.openConnection(proxy);
	} else {
	    ucon = url.openConnection();
	}
	ucon.addRequestProperty("User-agent", this.userAgent);

	return ((HttpURLConnection) ucon);
    }

    /**
     * Method created to ignore the verification of the server certificate used
     * on SSL. To very the certificate is necessary to have a trusted keystore
     * to support this operation
     * 
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    protected void confidInavlidSSL() throws NoSuchAlgorithmException,
	    KeyManagementException {
	final SSLContext ctx = SSLContext.getInstance("TLS");
	ctx.init(new KeyManager[0],
		new TrustManager[] { new X509TrustManager() {

		    public void checkClientTrusted(
			    final X509Certificate[] arg0, final String arg1)
			    throws CertificateException {
		    }

		    public void checkServerTrusted(
			    final X509Certificate[] arg0, final String arg1)
			    throws CertificateException {
		    }

		    public X509Certificate[] getAcceptedIssuers() {
			return null;
		    }

		}

		}, new SecureRandom());

	SSLContext.setDefault(ctx);
    }

    public void setUseProxy(final boolean useProxy) {
	this.useProxy = useProxy;
    }

    public String getUserAgent() {
	return userAgent;
    }

    public void setUserAgent(final String userAgent) {
	this.userAgent = userAgent;
    }

    public String get_urlStr() {
	return _urlStr;
    }

    public void set_urlStr(final String _urlStr) {
	this._urlStr = _urlStr;
    }

    public String getDorkIn() {
	return dorkIn;
    }

    public void setDorkIn(final String dorkIn) {
	this.dorkIn = dorkIn;
    }

    public String getDorkNotIn() {
	return dorkNotIn;
    }

    public void setDorkNotIn(final String dorkNotIn) {
	this.dorkNotIn = dorkNotIn;
    }

    public int getMaxDork() {
	return maxDork;
    }

    public void setMaxDork(final int maxDork) {
	this.maxDork = maxDork;
    }

    public int getMaxEmptyPage() {
	return maxEmptyPage;
    }

    public void setMaxEmptyPage(final int maxEmptyPage) {
	this.maxEmptyPage = maxEmptyPage;
    }

    public int getMaxPage() {
	return maxPage;
    }

    public void setMaxPage(final int maxPage) {
	this.maxPage = maxPage;
    }

    public int getPageSize() {
	return pageSize;
    }

    public void setPageSize(final int pageSize) {
	this.pageSize = pageSize;
    }

    public String getRegex() {
	return regex;
    }

    public void setRegex(final String regex) {
	this.regex = regex;
    }

    public boolean isUseProxy() {
	return useProxy;
    }

}
