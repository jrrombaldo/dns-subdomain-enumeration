package br.jrrombaldo.enumeration.subdomain;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SearchEngine {

    protected static String target = null;
    protected static String proxy = null;
    protected static int pages = 0;
    protected static int engine = 0;

    protected static Set<String> results = new HashSet<String>();

    public static void main(String[] args) {

	try {

	    fillAttributes();

	    System.out.println("Executing...");
	    performQueries();

	    System.out.println("Results:");
	    for (String result : results)
		System.out.println("\t" + result);

	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    private static void performQueries() throws Exception {
	if (engine == 1 || engine == 3) {
	    GoogleSearch search = new GoogleSearch(target, pages);
	    results.addAll(search.getAllSubDomains());
	}

	if (engine == 2 || engine == 3) {
	    BingSearch search = new BingSearch(target, pages);
	    results.addAll(search.getAllSubDomains());
	}
    }

    private static void fillAttributes() {
	Scanner scan = new Scanner(System.in);

	try {
	    while (target == null || target.length() < 5) {
		System.out.println("Which is the target domain (ex: cnn.com)");
		target = scan.next();
	    }

	    System.out
		    .println("Use HTTP Proxy?  (NO-<Any Key>Enter Yes-<Proxy>");
	    proxy = scan.next();

	    while (pages <= 1) {
		System.out
			.println("Number of pages to be considered per query (25 is a good)");
		pages = scan.nextInt();
	    }

	    while (engine < 1 || engine > 3) {
		System.out.println("Search Engine (1-Google; 2-Bing; 3-Both)");
		engine = scan.nextInt();
	    }
	} finally {
	    if (scan != null)
		scan.close();
	}
    }
}
